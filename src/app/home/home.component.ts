import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
    title = 'i-boxer-iu';
    users: Array<User> = [];
    posts: Array<any> = [];

    constructor(private httpClient: HttpClient) { }

    ngOnInit() {
        this.httpClient.get<Array<User>>(environment.apiUrl + 'users').subscribe(result => {
            this.users = result;
        },
            error => {
                alert(JSON.stringify(error));
            });
    }
}

export class User {
    firstName: string;
    lastName: string;
}